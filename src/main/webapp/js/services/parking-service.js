function ParkingService() {
}

ParkingService.prototype = Object.create(RestService.prototype);

/**
 *
 * @returns {Promise<Array<{id: number, address: string, places: {}}>>}
 */
ParkingService.prototype.getAllParkingLots = function () {
    return this.get(["parking"]);
};

/**
 *
 * @param {*} parkingLot
 * @returns {Promise<*>}
 */
ParkingService.prototype.addParkingLot = function (parkingLot) {
    return this.post(["parking"], parkingLot);
};
