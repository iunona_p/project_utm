function RestService() {
}

/**
 *
 * @param {{ url: Array<*>, method: string }} props
 * @param {*} [props.data={}]
 * @returns {Promise<*>}
 */
RestService.prototype.request = function (props) {
    var settings = {
        url: "/" + props.url.join("/"),
        method: props.method,
        data: JSON.stringify(props.data),
        headers: {
            "Content-Type": "application/json"
        }
    };

    return $.ajax(settings)
};

/**
 *
 * @param {Array<*>} url
 * @returns {Promise<*>}
 */
RestService.prototype.get = function (url) {
    return this.request({
        url: url,
        method: "GET"
    })
};

/**
 *
 * @param {Array<*>} url
 * @param {*} [params={}]
 * @returns {Promise<*>}
 */
RestService.prototype.post = function (url, params) {
    return this.request({
        url: url,
        data: params,
        method: "POST"
    })
};

/**
 *
 * @param url Array<*>
 * @param params {*}
 * @returns Promise<*>
 */
RestService.prototype.put = function (url, params) {
    return this.request({
        url: url,
        data: params,
        method: "PUT"
    })
};

/**
 *
 * @param {Array<*>} url
 * @param {*} [params={}]
 * @returns {Promise<*>}
 */
RestService.prototype.put = function (url, params) {
    return this.request({
        url: url,
        data: params,
        method: "PATCH"
    })
};
