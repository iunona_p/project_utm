$(document).ready(function () {
    parking.getAllParkingLots()
        .then(function (value) {
            renderTabs(value)
        });
});

$(document).on('click', '#add-parking', function () {
    parking.addParkingLot({address: "default address"})
        .then(function () {
            parking.getAllParkingLots()
                .then(function (value) {
                    renderTabs(value)
                });
        })
});