function renderTabs(value) {
    var $pillsTab = $("#v-pills-tab");
    $pillsTab.empty();
    $("#v-pills-tabContent").empty();

    $pillsTab.prepend("<a class=\"nav-link\" id=\"add-parking\" role=\"button\" aria-selected=\"true\"><strong>+</strong></a>");

    value.forEach(function (v, index) {
        $pillsTab.prepend(
            "<a class=\"nav-link " + (index === 0 ? "active" : "")
            + "\" id=\"v-pills-" +
            v.id
            + "-tab\" data-toggle=\"pill\" href=\"#v-pills-" +
            v.id
            + "\" role=\"tab\" aria-controls=\"v-pills-settings\" aria-selected=\"false\">" +
            v.address +
            "</a>"
        );

        var places = "";
        for (var p in v.places) {
            places += "<li>Place: " + v.places[p].id + "</li>"
        }
        $("#v-pills-tabContent").append(
            "<div class=\"tab-pane " +
            (index === 0 ? "show active" : "fade") + "\" id=\"v-pills-" +
            v.id
            + "\" role=\"tabpanel\" aria-labelledby=\"v-pills-" +
            v.id
            + "-tab\"><ul>" +
            places,
            "</ul></div>"
        )
    });
}
