package com.example.utm.demo.parking.services;

import com.example.utm.demo.parking.models.Car;
import com.example.utm.demo.parking.models.ParkingLot;
import com.example.utm.demo.parking.models.ParkingPlace;
import com.example.utm.demo.parking.repositories.CarRepository;
import com.example.utm.demo.parking.repositories.ParkingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParkingServiceImpl implements ParkingService {

    private CarRepository carRepository;

    private ParkingRepository parkingRepository;

    @Override
    public ParkingPlace getParkingPlace(Long placeId) {
        return null;
    }

    @Override
    public List<ParkingPlace> getAllParkingPlacesByParkingLotId(Long parkingPlaceId) {
        return parkingRepository.getAllParkingPlacesByParkingLotId(parkingPlaceId);
    }

    @Override
    public ParkingLot getParkingLot(Long parkingLotId) {
        return parkingRepository.getParkingLot(parkingLotId);
    }

    @Override
    public List<ParkingLot> getAllParkingLots() {
        return parkingRepository.getAllParkingLots();
    }

    @Override
    public void store(ParkingLot parkingLot) {
        parkingRepository.store(parkingLot);
    }

    @Override
    public void assignCar(Long placeId, Long carId) {
        Car car = carRepository.getById(carId);
        parkingRepository.assignCar(placeId, car);
    }

    @Autowired
    public void setCarRepository(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Autowired
    public void setParkingRepository(ParkingRepository parkingRepository) {
        this.parkingRepository = parkingRepository;
    }
}
