package com.example.utm.demo.parking.repositories;

import com.example.utm.demo.parking.models.Car;
import com.example.utm.demo.parking.models.ParkingLot;
import com.example.utm.demo.parking.models.ParkingPlace;

import java.util.List;

public interface ParkingRepository {
    ParkingPlace getParkingPlace(Long placeId);

    List<ParkingPlace> getAllParkingPlacesByParkingLotId(Long parkingPlaceId);

    ParkingLot getParkingLot(Long parkingLotId);

    List<ParkingLot> getAllParkingLots();

    void store(ParkingLot parkingLot);

    void assignCar(Long placeId, Car car);
}
