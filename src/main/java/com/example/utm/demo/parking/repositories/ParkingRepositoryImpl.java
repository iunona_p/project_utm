package com.example.utm.demo.parking.repositories;

import com.example.utm.demo.parking.models.Car;
import com.example.utm.demo.parking.models.ParkingLot;
import com.example.utm.demo.parking.models.ParkingPlace;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ParkingRepositoryImpl implements ParkingRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ParkingPlace getParkingPlace(Long placeId) {
        return entityManager.find(ParkingPlace.class, placeId);
    }

    @Override
    public List<ParkingPlace> getAllParkingPlacesByParkingLotId(Long parkingLotId) {
        return entityManager
                .createQuery("select pp from ParkingPlace pp where pp.parkingLot.id = :parkingLotId", ParkingPlace.class)
                .setParameter("parkingLotId", parkingLotId)
                .getResultList();
    }

    @Override
    public ParkingLot getParkingLot(Long parkingLotId) {
        return entityManager.find(ParkingLot.class, parkingLotId);
    }

    @Override
    public List<ParkingLot> getAllParkingLots() {
        return entityManager.createQuery("select pl from ParkingLot pl", ParkingLot.class).getResultList();
    }

    @Override
    @Transactional
    public void store(ParkingLot parkingLot) {
        entityManager.merge(parkingLot);
    }

    @Override
    public void assignCar(Long placeId, Car car) {
        ParkingPlace pp = entityManager.find(ParkingPlace.class, placeId);
        pp.setAssignedCar(car);
        entityManager.merge(pp);
    }
}
