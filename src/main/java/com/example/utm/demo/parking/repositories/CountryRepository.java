package com.example.utm.demo.parking.repositories;

import com.example.utm.demo.parking.models.Country;
import org.springframework.data.repository.CrudRepository;

public interface CountryRepository extends CrudRepository<Country, Long> {


}
