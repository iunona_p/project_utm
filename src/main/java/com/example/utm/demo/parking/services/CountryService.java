package com.example.utm.demo.parking.services;

import com.example.utm.demo.parking.models.Country;
import com.example.utm.demo.parking.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryService {
    @Autowired
    CountryRepository countryRepository;

    public List<Country> getAllCountries(){

        return (List)countryRepository.findAll();
    }



}
