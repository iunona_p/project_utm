package com.example.utm.demo.parking.repositories;

import com.example.utm.demo.parking.models.Car;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository {
    void store(Car car);

    Car getById(Long id);

    List<Car> getAll();

    List<Car> getAllCarsByCountry (Long id);
}
