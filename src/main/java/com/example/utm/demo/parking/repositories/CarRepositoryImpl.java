package com.example.utm.demo.parking.repositories;

import com.example.utm.demo.parking.models.Car;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class CarRepositoryImpl implements CarRepository {

    final static Logger logger = LogManager.getLogger(CarRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void store(Car car) {
        entityManager.persist(car);
    }

    @Override
    public Car getById(Long id) {
        logger.info("Car with id " + id + " is found");
        return entityManager.find(Car.class, id);
    }

    @Override
    public List<Car> getAll() {
        return entityManager.createQuery("select c from Car c", Car.class)
                .getResultList();
    }

    @Override
    public List<Car> getAllCarsByCountry(Long id) {

        Query query = entityManager.createNativeQuery("SELECT c.* FROM car c LEFT JOIN country c2 on c.country_id = c2.id LEFT JOIN driver d2 on c.driver_id = d2.id WHERE c2.id = ?;", Car.class)
                .setParameter(1, id);

        List<Car> queryResult = query.getResultList();

        for (Object c : queryResult) {
            logger.info("Car " + ((Car) c).getNumber() + " model: " + ((Car) c).getModel() + " registered in: " + ((Car) c).getCountry());
        }

        return queryResult;
    }


}
