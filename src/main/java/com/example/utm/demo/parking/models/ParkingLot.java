package com.example.utm.demo.parking.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "parking_lot")
public class ParkingLot {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "address")
    private String address;

    @OneToMany
    @JoinColumn(name = "parking_lot_id")
    private Set<ParkingPlace> places;

    public ParkingLot() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<ParkingPlace> getPlaces() {
        return places;
    }

    public void setPlaces(Set<ParkingPlace> places) {
        this.places = places;
    }
}
