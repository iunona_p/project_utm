package com.example.utm.demo.controllers;


import com.example.utm.demo.parking.models.Country;
import com.example.utm.demo.parking.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CountryController {

    @Autowired
    CountryService countryService;

    @RequestMapping(value = "/countries", method = RequestMethod.GET)
    public ResponseEntity<List<Country>> countries(){
        return new ResponseEntity<>(countryService.getAllCountries(), HttpStatus.OK);
    }


}
