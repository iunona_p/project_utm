package com.example.utm.demo.controllers;

import com.example.utm.demo.parking.models.ParkingLot;
import com.example.utm.demo.parking.models.ParkingPlace;
import com.example.utm.demo.parking.services.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/parking")
public class ParkingController {

    private ParkingService parkingService;

    @RequestMapping(value = "/place/{placeId}", method = RequestMethod.GET)
    public ParkingPlace getParkingPlace(@PathVariable Long placeId) {
        return parkingService.getParkingPlace(placeId);
    }

    @RequestMapping(value = "/places-by-parking-lot-id/{parkingPlaceId}", method = RequestMethod.GET)
    public List<ParkingPlace> getAllParkingPlacesByParkingLotId(@PathVariable Long parkingPlaceId) {
        return parkingService.getAllParkingPlacesByParkingLotId(parkingPlaceId);
    }

    @RequestMapping(value = "/place", method = RequestMethod.GET)
    public List<ParkingPlace> getAllParkingPlaces(@PathVariable Long parkingLotId) {
        return parkingService.getAllParkingPlacesByParkingLotId(parkingLotId);
    }

    @RequestMapping(value = "/{parkingLotId}", method = RequestMethod.GET)
    public ParkingLot getParkingLot(@PathVariable Long parkingLotId) {
        return parkingService.getParkingLot(parkingLotId);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<ParkingLot> getAllParkingLots() {
        return parkingService.getAllParkingLots();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public void store(@RequestBody ParkingLot parkingLot) {
        parkingService.store(parkingLot);
    }

    @RequestMapping(value = "/place/{placeId}/assign/{carId}", method = RequestMethod.POST)
    public void assignCar(@PathVariable Long placeId,
                          @PathVariable Long carId) {
        parkingService.assignCar(placeId, carId);
    }

    @Autowired
    public void setParkingService(ParkingService parkingService) {
        this.parkingService = parkingService;
    }

}
