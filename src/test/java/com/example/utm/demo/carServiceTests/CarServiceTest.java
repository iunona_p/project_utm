package com.example.utm.demo.carServiceTests;

import com.example.utm.demo.parking.models.Car;
import com.example.utm.demo.parking.models.Country;
import com.example.utm.demo.parking.repositories.CarRepository;
import com.example.utm.demo.parking.repositories.CarRepositoryImpl;
import com.example.utm.demo.parking.services.CarService;
import com.example.utm.demo.parking.services.CarServiceImpl;
import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class CarServiceTest {

    final static Logger logger = LogManager.getLogger(CarServiceTest.class);

    @Test
    public void getAllCarsByCountry () {

        Country country = new Country("Moldova");
        country.setId(1L);

        Car car = new Car(1L, null, "367", "renault");
        car.setCountry(country);
        Car car1 = new Car(1L, null, "358", "peugeot");
        car1.setCountry(country);

        List<Car> carsToTest = new ArrayList<>();
        carsToTest.add(car); carsToTest.add(car1);


        CarService carService = new CarServiceImpl();
        ((CarServiceImpl) carService).setCarRepository(new CarRepository() {
            @Override
            public void store(Car car) {
            }

            @Override
            public Car getById(Long id) {
                return car;
            }

            @Override
            public List<Car> getAll() {
                return null;
            }

            @Override
            public List<Car> getAllCarsByCountry (Long id) {
                logger.info("I was here ");
                return carsToTest;
            }
        });

        assertEquals (carsToTest, carService.getCarsByCountryId(1L));
    }

}
