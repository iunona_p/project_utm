package com.example.utm.demo.parking.services;

import com.example.utm.demo.parking.models.Car;
import com.example.utm.demo.parking.models.ParkingLot;
import com.example.utm.demo.parking.models.ParkingPlace;
import com.example.utm.demo.parking.repositories.ParkingRepository;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ParkingServiceImplTest {

	@Test
	public void getParkingPlace() {
		ParkingService s = new ParkingServiceImpl();
		ParkingPlace pp = s.getParkingPlace(1L);

		assertNull(pp);
	}

	@Test
	public void getAllParkingPlacesByParkingLotId() {
		List <ParkingPlace> parkingPlaces = new ArrayList<ParkingPlace>();
		parkingPlaces.add(new ParkingPlace(1L));
		parkingPlaces.add(new ParkingPlace(2L));

		ParkingService s = new ParkingServiceImpl();
		((ParkingServiceImpl) s).setParkingRepository(new ParkingRepository() {
			public ParkingPlace getParkingPlace(Long placeId) {
				return null;
			}

			public List<ParkingPlace> getAllParkingPlacesByParkingLotId(Long parkingPlaceId) {
				return parkingPlaces;
			}

			public ParkingLot getParkingLot(Long parkingLotId) {
				return null;
			}

			public List<ParkingLot> getAllParkingLots() {
				return null;
			}

			public void store(ParkingLot parkingLot) {
			}

			public void assignCar(Long placeId, Car car) {
			}
		});

		List<ParkingPlace> pp = s.getAllParkingPlacesByParkingLotId(1L);

		assertEquals(parkingPlaces, pp);

	}

	@Test
	public void getParkingLot() {
	}

	@Test
	public void getAllParkingLots() {
	}

	@Test
	public void store() {
	}

	@Test
	public void assignCar() {
	}

	@Test
	public void setCarRepository() {
	}

	@Test
	public void setParkingRepository() {
	}
}